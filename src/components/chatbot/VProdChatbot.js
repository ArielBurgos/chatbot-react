import React, { Component } from 'react';
// import { findDOMNode } from 'react-dom';
import ChatBot from 'react-simple-chatbot';
import { ThemeProvider } from 'styled-components';
import JiraLookup from '../atlassian/JiraLookup';
import ConfLookup from '../atlassian/ConfLookup';

// all available theme props
const theme = {
  background: '#f5f8fb',
  fontFamily: 'Verdana',
  headerBgColor: '#267897',
  headerFontColor: '#fff',
  headerFontSize: '15px',
  botBubbleColor: '#267897',
  botFontColor: '#fff',
  userBubbleColor: '#fff',
  userFontColor: '#4a4a4a',
};

const config ={
    width: "500px",
    height: "600px", 
    floating: true,
  };



class VProdChatbot extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <ChatBot 
          steps={[
            
                {
                  id:'intro', 
                  message:'By using this chatbot, you agree to the Terms and Conditions outlined at <JustGoogleItOrSomethingIDK>', 
                  trigger:'intro-user',
                  
                },
                {
                  id:'intro-user', 
                  options:[
                  {value:'y', label:'Yes, I Accept', trigger:'yes-response'},
                  {value:'n', label:'No, I Do Not Accept', trigger:'no-response'},
                  ] 
                },
                {
                  id:'yes-response', 
                  message:'How can I help you today?', 
                  trigger:'options'
                },
                {
                  id:'options',
                  options:[
                    {value:'jira', label:'Search JIRA', trigger:'jira'},
                    {value:'confluence', label:'Search Confluence', trigger:'confluence'},
                    ]
                },
                {
                  id:'jira', 
                  message:'What would you like to do?', 
                  trigger:'jira-options',
                },
                {
                  id:'jira-options', 
                  options:[
                  {value:'tickets', label:'Search JIRA Tickets', trigger:'tickets'},
                  {value:'jql', label:'Search JIRA Using JQL', trigger:'jql'},
                  ]
                },
                {
                  id:'tickets', 
                  message:'Please Enter the Ticket ID', 
                  trigger:'ticketsUser',
                },
                {
                  id:'jql', 
                  message:'Please Enter a valid JQL query', 
                  trigger:'jqlUser',
                },
                {
                  id:'confluence', 
                  message:'What would you like to do?', 
                  trigger:'conf-options',
                },
                {
                  id:'conf-options', 
                  options:[
                  {value:'tickets', label:'Search Confluence Articles', trigger:'articles'},
                  {value:'jql', label:'Search Confluence Using CQL', trigger:'cql'},
                  ] 
                },
                {
                  id:'articles', 
                  message:'Please Enter the Article Name', 
                  trigger:'articlesUser',
                },
                {
                  id:'cql', 
                  message:'Please Enter a valid CQL query', 
                  trigger:'cqlUser',
                },
                {
                  id:'ticketsUser',
                  user:true,
                  trigger:'jira-2',
                },
                {
                  id:'jqlUser',
                  user:true,
                  trigger:'jira-2',
                },
                {
                  id:'articlesUser',
                  user:true,
                  trigger:'confluence-2',
                },
                {
                  id:'cqlUser',
                  user:true,
                  trigger:'confluence-2',
                },
                
                {
                  id:'jira-2',
                  component: <JiraLookup />,
                  // waitAction: true,
                  // trigger: '1',
                  // message: "Neat",
                  end:true,
                  
                },
                {
                  id:'confluence-2',
                  // component: <ConfLookup />,
                  // waitAction: true,
                  // trigger: '1',
                  message: "Awesome",
                  end:true,
                },
                {
                  id:'no-response', 
                  message:'Please refer to the Terms and Conditions at vprod.io', 
                  end:true,
                },
            
            
          ]}
          {...config}
          />
        </ThemeProvider>
        );
      }

    }

    export default VProdChatbot;