import React, { Component } from 'react';
import axios from 'axios';
import { apiOptions } from '../../config';


async function getTickets(){

    var response = await axios.get(apiOptions.aws + '/', {
        headers: {
            'Authorization': 'Basic dnByb2Quc2VydmljZUBnbWFpbC5jb206MUZUZklMS0pXcTdJZnVmY291ZXQyMjRB'
          }
      })

      console.log(response.data);

      var fields = response.data.fields;

      console.log("TICKET KEY: " + response.data.key)
      console.log("TICKET CREATOR: " + fields.creator.displayName)
      console.log("TICKET REPORTER: " + fields.reporter.displayName)
      console.log("TICKET ASSIGNEE: " + fields.assignee.displayName)
      console.log("STATUS: " + fields.status.name)
      console.log("SUMMARY: " + fields.summary)
      console.log("SUBTASKS: " + fields.subtasks)

      return fields;

}

class JiraLookup extends Component {
    constructor(props) {
        super(props);

        const { steps } = this.props;
        const { ticketsUser, jqlUser} = steps;

        this.state =  { ticketsUser, jqlUser, tickets : "base" }; 

     
    }

    async componentWillMount() {

        var result = getTickets();
        this.setState({
            tickets: JSON.stringify(result)
          });

          console.log("is the state " + this.state.tickets)
        //this.setState({ ticketsUser: users, groupList: groups, len:users.length })

        
    }
    
  
    async componentDidMount() {

        var result = getTickets();
        this.setState({
            tickets: JSON.stringify(result)
          });
        //this.setState({ ticketsUser: users, groupList: groups, len:users.length })

        
    }

    async componentDidUpdate() {
        //var result = getTickets();
        
    }
  
    render() {

    const defaultString = "defaultString";

    const tickets = this.state.tickets ?
      <h2>{this.state.tickets}</h2> : <h1>{defaultString}</h1>;

        return (
            <div>{tickets}</div>
        )
  
   }    
     
  };
  
  export default JiraLookup;